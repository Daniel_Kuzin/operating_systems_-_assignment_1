/* Copyright (c) 1997-2017, FenixOS Developers
   All Rights Reserved.

   This file is subject to the terms and conditions defined in
   file 'LICENSE', which is part of this source code package.
 */

/*! \file video.c This file holds implementations of functions
  presenting output to the VGA screen. */
#include <stdint.h>

/*! Max number of columns in the VGA buffer. */
#define MAX_COLS                (80)
/*! Max number of columns in the VGA buffer. */
#define MAX_ROWS                (25)

int CURRENT_COLS = 0;
int CURRENT_ROWS = 0;

struct screen_position
{
 unsigned char character; /*!< The character part of the byte tuple used for
                               each screen position. */
 unsigned char attribute; /*!< The character part of the byte tuple used for
                               each screen position. */
};
/*!< Defines a VGA text mode screen position. */

struct screen
{
 struct screen_position positions[MAX_ROWS][MAX_COLS];
 /*!< The VGA screen. It is organized as a two dimensional array. */
};
/*!< Defines a VGA text mode screen. */

/*! points to the VGA screen. */
static struct screen* const
screen_pointer = (struct screen*) 0xB8000;

void clear_screen() {
    for (int i = 0; i < MAX_COLS; i++) {
        for (int j = 0; j < MAX_ROWS; j++) {
            screen_pointer->positions[j][i].attribute = 0x0;
            screen_pointer->positions[j][i].character = 0x0;
        }
    }
    CURRENT_ROWS = 0;
    CURRENT_COLS = 0;
}

void kprints(const char* string) {
    int i = 0;

    do {
        if(string[i] == '\n') {
            if(CURRENT_ROWS == MAX_ROWS) {
                clear_screen();
            } else {
                CURRENT_ROWS++;
            }
            CURRENT_COLS = 0;
        } else {
            if(CURRENT_COLS == MAX_COLS) {
                if(CURRENT_ROWS == MAX_ROWS) {
                    clear_screen();
                } else {
                    CURRENT_ROWS++;
                    CURRENT_COLS = 0;
                }
            }
            screen_pointer->positions[CURRENT_ROWS][CURRENT_COLS].character = string[i];
            screen_pointer->positions[CURRENT_ROWS][CURRENT_COLS].attribute = 0xF;
            CURRENT_COLS++;
        }
        i++;
    } while (string[i] != '\0');
}
char getHex(uint32_t result) {
    switch (result) {
        case 0x0:
            return '0';
        case 0x1:
            return '1';
        case 0x2:
            return '2';
        case 0x3:
            return '3';
        case 0x4:
            return '4';
        case 0x5:
            return '5';
        case 0x6:
            return '6';
        case 0x7:
            return '7';
        case 0x8:
            return '8';
        case 0x9:
            return '9';
        case 0xA:
            return 'A';
        case 0xB:
            return 'B';
        case 0xC:
            return 'C';
        case 0xD:
            return 'D';
        case 0xE:
            return 'E';
        case 0xF:
            return 'F';
        default:
            return '?';
    }
}

void kprinthex(const register uint32_t value) {
    char string[11];

    int i = 9;

    uint32_t index = 0xF;

    uint32_t new_value = value;

    for (int j = 0; j < 8; j++) {
        uint32_t result = new_value & index;
        string[i] = getHex(result);
        i--;
        new_value = new_value >> 4;
    }
    string[0] = '0';
    string[1] = 'x';
    string[10] = '\0';

    kprints(string);
}
