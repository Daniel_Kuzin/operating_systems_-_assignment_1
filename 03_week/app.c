#include <stdio.h>
#include <malloc.h>

struct node {
    int value;
    struct node *prev;
};
struct node* append(struct node* latestNode, int counter) {
    struct node* newNode = (struct node*) malloc(sizeof(struct node));

    newNode->prev = latestNode;
    newNode->value = counter;

    return newNode;
}
struct node* pop(struct node* latestNode) {
    struct node* tmpNode = latestNode->prev;

    free(latestNode);

    return tmpNode;
}
void print(struct node* latestNode) {
    if (latestNode->prev != NULL) {
        print(latestNode->prev);
        printf(",%d",latestNode->value);
    } else {
        printf("%d",latestNode->value);
    }
    free(latestNode);
}
void program(struct node* latestNode, int counter) {
    char c;

    scanf(" %c", &c);

    if(c == 'a') {
        latestNode = append(latestNode, counter);
        counter++;
        program(latestNode, counter);
    } else if (c == 'b') {
        counter++;
        program(latestNode, counter);
    } else if (c == 'c') {
        latestNode = pop(latestNode);
        counter++;
        program(latestNode, counter);
    } else {
        if(latestNode == NULL) {
            printf("<blank>");
        } else {
            print(latestNode);
        }
        printf("\n");
    }
}
int main() {
    program(NULL, 0);
    return 0;
}
