#include <stdio.h>
#include <malloc.h>

struct node {
    int value;
    struct node *prev;
};
struct node* append(struct node* latestNode, int counter) {
    struct node* newNode = (struct node*) malloc(sizeof(struct node));

    newNode->prev = latestNode;
    newNode->value = counter;

    return newNode;
}
struct node* pop(struct node* latestNode) {
    struct node* tmpNode = latestNode->prev;

    free(latestNode);

    return tmpNode;
}
void print(struct node* latestNode) {
    if (latestNode->prev != NULL) {
        print(latestNode->prev);
        printf(",%d",latestNode->value);
    } else {
        printf("%d",latestNode->value);
    }
    free(latestNode);
}
int main() {
    struct node* latestNode = NULL;
    latestNode = append(latestNode,0);
    latestNode = append(latestNode,1);
    latestNode = append(latestNode,2);

    if(latestNode->value != 2) {
        printf("Error\n");
	return -1;
    }
    latestNode = pop(latestNode);
    if(latestNode->value != 1) {
        printf("Error\n");
	return -1;
    }
    latestNode = pop(latestNode);
    if(latestNode->value != 0) {
        printf("Error\n");
	return -1;
    }
    latestNode = pop(latestNode);
    if(latestNode != NULL) {
	printf("Error\n");
	return -1;
    }
    printf("Success\n");
    return 0;
}
